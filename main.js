//version 1.0
//LOAD MODULE SECTION
var deviceModule = require('./servicepack/device.js');
var mqttModule = require('./servicepack/_mqtt.js');



//OPERATE SECTION
//
var mqttClient = mqttModule.mqttEnable();
var getMqttStore = mqttModule.getMqttStore();

//subscribe mqtt topic
mqttClient.on('connect', function(){
   	mqttClient.subscribe('Anchor/#');
  	console.log('...MQTT subscribes on <Anchor/#>');

  	//mqttClient.subscribe(resetTopic);
	//console.log('...MQTT RESET topic: ' + resetTopic); 

 	//mqttClient.subscribe('hello');
	//console.log('...MQTT hello topic: ' + 'hello');
});

//on mqtt message
mqttClient.on('message', function(topic, payload){
    var message = payload.toString();
	console.dir(message,{depth:null, colors:true}); 
});