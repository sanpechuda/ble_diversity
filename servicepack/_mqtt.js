//V1.0
//last update 2019/01/17

var mqtt = require('mqtt');
var mqttBroker = 'mqtt://203.185.65.92:10001';
var clientId = 'mqttjs_' + Math.random().toString(32).substr(2, 5);
var machineId = require('node-machine-id').machineIdSync({original: true});

var mqttClient;
var mqttStore;

var willTopic = 'alive';
var willPayload = {};
willPayload.id = machineId;
willPayload.status = 'offline';

function mqttEnable(){
	mqttClient = mqtt.connect(mqttBroker, 
	    {
	 		clientId: clientId, 
	       	protocolId: 'MQIsdp', 
	     	protocolVersion: 3,
	     	reconnectPeriod: 1*1000, //ms
	      	connectTimeout: 3*1000, //ms
	      	debug:true,
	      	properties: {
	      		sessionExpiryInterval: 5,// second
	      		receiveMaximum: 1,
	      	},
	      	will: {
	      		topic: willTopic + '/xxx',
	      		payload: JSON.stringify(willPayload),
	      		qos: 2,
	      		retain: true
	      	}
	   	}
	);

	mqttClient.on('connect', function(){
		//alive signal on connect
		mqttClient.publish(willTopic + '/xxx', JSON.stringify({abc: 'i alive'}) , {retain: true});

	});

	//forward will
	forwardWill(mqttClient);

	return mqttClient;
}

function getMqttStore(){
	mqttStore = mqtt.Store({
		//options
		//clean: true
	});
	return mqttStore;
}

function forwardWill(mqttClient){
	//subscribe will topic
	mqttClient.subscribe(willTopic + '/#');
	mqttClient.on('message', function(topic, payload){
		var message = payload.toString();
		var jsonData;

		if(!topic.includes(willTopic)){
			return;
		}

		try{
            jsonData = JSON.parse(message);
            //console.dir(jsonData,{depth:null, colors:true}); 
        }catch(err){
            console.log('Error: cannot parse JSON');
            return;
        }

		if(jsonData && jsonData.status == 'offline' && !jsonData.forward){
			jsonData.forward = true;
			jsonData.time = getUnixTime();
			mqttClient.publish('alive/xxx', JSON.stringify(jsonData) , {retain: true});
		}
	});	
}

function getTimestamp(){
	// var now = new time.Date();
	var now = new Date();
	//now.setTimezone('UTC', true);
	return now.toString();
}

function getUnixTime(){
    var date = new Date();
    var milliUnixTime = date.getTime(); //in milliseconds
    return Math.round(milliUnixTime/1000); //in seconds
}


var methods = {};
methods.mqttEnable = mqttEnable;
methods.getMqttStore = getMqttStore;

//exports.methods = methods;
module.exports = methods;