var mysql = require('mysql');
/*var mysqlConnection = mysql.createConnection({
	host     : '203.185.65.92', //ino server
	port     : 10003,
 	user     : 'unai',
  	password : 'unaiunai',
  	database : 'UNAI_BLE_2018'
});*/
var pool = mysql.createPool({
	connectionLimit : 10, // default = 10
	host     : '203.185.65.92', //ino server
	port     : 10003,
 	user     : 'unai',
  	password : 'unaiunai',
  	//database : 'UNAI_BLE_DB' //arno
  	database : 'rtls_ble' //noom
});

function runSql(sqlCommand, callback){
	pool.getConnection(function(err, connection){
		if(err){
			console.error('error connecting: ' + err.stack);
    		callback(err, null, null);
		}
		//console.log('mysql is connected as id ' + mysqlConnection.threadId);

		connection.query(sqlCommand, function (error, results, fields){
			connection.release();
			callback(error, results, fields);
		});
	});
}

var methods = {};
methods.runSql = runSql;

//exports.methods = methods;
module.exports = methods;
