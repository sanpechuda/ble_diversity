//V1.0
//last update 2019/01/17

//init devices
var devices = {};
devices.anchor = {};
devices.tag = {};
devices.joinUser = {};
devices.floor = {};

//var mysqlService = require('../service.js').mysql;
var mysqlService = require('../servicepack/_mysql.js');
//var sql = 'SELECT * FROM `anchor` WHERE `building_id`=6'; //arno
var sql = 'SELECT * FROM `anchor` WHERE `floor_id` IN (SELECT `id` FROM floor WHERE `building_id` = 1)'; //noom

async function loadProfiles(callback){
	//anchors = await loadAnchors();
	//tags = await loadTags();
	arrayResults1 = await Promise.all([
		loadAnchors(),
		loadTags(),
		loadFloors(1),
	]);
	arrayResults2 = await loadJoinUsers();

	//console.dir(arrayResults2, {depth:null, colors:true});
	callback(arrayResults1, arrayResults2);
}

function loadAnchors(){
	return new Promise((resolve, reject)=>{
		mysqlService.runSql('SELECT * FROM `anchor`', function(error, results, fields){
			if(error){
				reject(error);
			}
			results.forEach(function(anchor, anchorIndex){
			 	//arno
			 	//let label = anchor.name;
			 	//let uuid = anchor.anchor_uuid;

			 	//noom
			 	let label = anchor.label;
			 	let uuid = anchor.mac_address_to_engine;

			 	let major = null;
			 	let minor = null;
			 	let level = anchor.floor_id;
			 	let location = [anchor.x, anchor.y, anchor.z];
			 	addAnchor(label, uuid, major, minor, level, location);
			});
			//console.dir(results, {depth:null, colors:true});
			console.log('...Imported ' + results.length + ' anchors');
			resolve(results);
		});
	});
}

function loadTags(){
	return new Promise((resolve, reject)=>{
		mysqlService.runSql('SELECT * FROM `tag`', function(error, results, fields){
			if(error){
				reject(error);
			}
			results.forEach(function(tag, tagIndex){
				let type = 'default';
		 		let label = tag.name;
			 	let uuid = tag.tag_id.substring(0,36);
			 	let major = tag.major;
			 	let minor = tag.minor;
			 	let level = null;
			 	let location = [null, null, null];
			 	addTag(type, label, uuid, major, minor, level, location);
			});
			//console.dir(results, {depth:null, colors:true});
			console.log('...Imported ' + results.length + ' tags');
			resolve(results);
		});
	});
}

function loadJoinUsers(){
	return new Promise((resolve, reject)=>{
		mysqlService.runSql('SELECT *, user.id as userId FROM user JOIN tag ON tag.id=user.tag_id', function(error, results, fields){
			if(error){
				reject(error);
			}
			//console.dir(results, {depth:null, colors:true});
			results.forEach(function(user, userIndex){
				let firstName = user.first_name;
				let lastName = user.last_name;
				let nickName = user.nick_name;
				let userId = user.userId;
				let gender = user.gender;
				let email = user.email;
				let birthday = user.birthday;
				let uuid = user.uuid;
				let major = user.major;
				let minor = user.minor;
				let initFloor = user.init_floor;
				if(user.last_floor == 0){
					user.last_floor = null;
				}
				let lastFloor = user.last_floor;
				let status = user.status;
				let altStatus = user.alt_status;
				addJoinUser(firstName, lastName, nickName, userId, gender, email, birthday, uuid, major, minor, initFloor, lastFloor, status, altStatus);
			});
			console.log('...Imported ' + results.length + ' users');
			//console.dir(user, {depth:null, colors:true});
			resolve(results);
		});
	});
}

function loadFloors(buildingId){
	return new Promise((resolve, reject)=>{
		mysqlService.runSql('SELECT * FROM floor where building_id=' + buildingId, function(error, results, fields){
			if(error){
				reject(error);
			}
			results.forEach(function(floor, floorIndex){
		 		let id = floor.id;
		 		let name = floor.name;

		 		if(!devices.floor.hasOwnProperty(id)){
		 			devices.floor[id] = {};
		 			devices.floor[id].id = id;
    				devices.floor[id].name = name;
    				devices.floor[id].users = [];
		 		}else{
		 			console.log('WARNING!... there are duplicated floor');
		 		}
			});
			//console.dir(results, {depth:null, colors:true});
			console.log('...Imported ' + results.length + ' floors');
			resolve(results);
		});
	});
}

function getObjectSize(obj){
	return Object.keys(obj).length;
}

function updateUserOnTag(tagId){
	if(devices.tag.hasOwnProperty(tagId)){
		devices.tag[tagId].user = devices.joinUser[tagId];
		devices.tag[tagId].lastUpdate = getUnixTime();
		return true;
	}
	return false;
}

function updateUserOnFloor(floorId, userId){
	if(devices.floor.hasOwnProperty(floorId)){
		devices.floor[floorId].users.push(userId);
		devices.floor[floorId].lastUpdate = getUnixTime();
	}
	return true;
}

function updateStatusUser(minor, options){
	let users = filterJoinUser({'minor': minor});
	if(users.length > 1){
		console.log('!!! ERROR(updateStatusUser)...Duplicated minors on user');
		return false;
	}
	
	for(var key in options){
		users[0][key] = options[key];
	}
	return true;
}

function addJoinUser(firstName, lastName, nickName, userId, gender, email, birthday, uuid, major, minor, initFloor, lastFloor, status, altStatus){
	var id = uuid+'-'+major+'-'+minor; //userId = tagId
	if(!devices.joinUser.hasOwnProperty(id)){
		devices.joinUser[id] = {};
		devices.joinUser[id].firstName = firstName;
		devices.joinUser[id].lastName = lastName;
		devices.joinUser[id].nickName = nickName;
		devices.joinUser[id].userId = userId;
		devices.joinUser[id].gender = gender;
		devices.joinUser[id].email = email;
		devices.joinUser[id].birthday = birthday;
		devices.joinUser[id].initFloor = initFloor;
		devices.joinUser[id].lastFloor = lastFloor;
		devices.joinUser[id].minor = minor;
		devices.joinUser[id].status = status;
		devices.joinUser[id].altStatus = altStatus;
		updateUserOnTag(id);
		updateUserOnFloor(initFloor, id);
		//console.dir(devices.tag[id], {depth:null, colors:true});
		return true;
	}else{
		console.log('WARNING!... There is duplicated user(same tag Id)');
		console.log(id);
		return false;
	}
}

function addTag(type, label, uuid, major, minor, level, location){
  //location is array, [x,y,z]
  var id = uuid+'-'+major+'-'+minor;
  //check if tag is not exist
  if(!devices.tag.hasOwnProperty(id)){
    //add new anchor
    devices.tag[id] = {};
    devices.tag[id].tagId = id;
    devices.tag[id].label = label;
    devices.tag[id].uuid = uuid;
    devices.tag[id].major = major;
    devices.tag[id].minor = minor;
    devices.tag[id].level = level;
    devices.tag[id].location = location;
    devices.tag[id].type = type;
    devices.tag[id].token = 0;
    devices.tag[id].lastUpdate = getUnixTime();
    devices.tag[id].isActive = true;
    devices.tag[id].user = null;
    return true;
  }
  return false;
}

function addAnchor(label, uuid, major, minor, level, location){
  //location is array, [x,y,z]
  var id = uuid;
  //check if anchor is not exist
  if(!devices.anchor.hasOwnProperty(id)){
    //add new anchor
    devices.anchor[id] = {};
    devices.anchor[id].anchorId = id;
    devices.anchor[id].label = label;
    devices.anchor[id].uuid = uuid;
    devices.anchor[id].major = major;
    devices.anchor[id].minor = minor;
    devices.anchor[id].level = level;
    devices.anchor[id].location = location;
    devices.anchor[id].n = 2.5;
    devices.anchor[id].topic = 'Anchor/'+uuid;
    //devices.anchor[id].status = 'inactive';
    devices.anchor[id].isActive = false;
    devices.anchor[id].lastUpdate = getUnixTime();
    return true;
  }
  return false;
}

function checkAnchor(anchorId){
	if(devices.anchor.hasOwnProperty(anchorId)){
		return true;
	}
	return false;
}

function getJoinUser(tagId){
	if(tagId){
		if(devices.joinUser.hasOwnProperty(tagId)){
			return devices.joinUser[tagId];
		}else{
			return null;
		}
	}else{
		// return all paired users
		return devices.joinUser;
	}	
}

function setUserLastFloor(userId, floorId){
	if(devices.joinUser.hasOwnProperty(userId)){
		let oldFloor = devices.joinUser[userId].lastFloor;
		if(floorId != oldFloor){
			//update database
			let datas = userId.split('-');
  			let minor = datas[datas.length-1];
			let sql = 'UPDATE user SET `last_floor` = '+ floorId +' WHERE `tag_id` = ' + minor;
			//console.log(sql);
			mysqlService.runSql(sql, function(error, results, fields){
				if(error){
					console.log('Error in function setUserLastFloor');
					console.log(error)
				}
				//console.log(oldFloor);
				//console.log(floorId);
				//console.log('Update last floor of Minor: ' + minor);
			});
		}
		//update variable
		devices.joinUser[userId].lastFloor = floorId;
		//console.log(devices.joinUser[userId]);
		return devices.joinUser[userId];
	}
	return false;
}

function filterJoinUser(schema){
	let users = Object.values(getJoinUser());
	let filterUsers = users.filter(function(user){
		for(var key in schema){
			if(user[key] != schema[key]){
				return false; 
			}
		}
        return true;
    });
    //console.log(filterUsers);
    return filterUsers;
}

function filterArray(refArray, schema){
	let results = refArray.filter(function(element){
		for(var key in schema){
			if(element[key] != schema[key]){
				return false; 
			}
		}
        return true;
	});
	return results;
}

function getFloor(floorId){
	if(floorId){
		if(devices.floor.hasOwnProperty(floorId)){
			return devices.floor[floorId];
		}else{
			return null;
		}
	}else{
		return devices.floor;
	}
}

function getUserOnFloor(floorId){
	if(floorId){
		if(devices.floor.hasOwnProperty(floorId)){
			return devices.floor[floorId].users;
		}else{
			return null;
		}
	}else{
		//users on all floor
		var allUsers = [];
		for(var key in devices.floor){
			if(devices.floor[key].users.length > 0){
				allUsers = allUsers.concat(devices.floor[key].users);
			}
		}
		return allUsers;
	}
}

function updateAnchor(uuid){
	var id = uuid;
	if(devices.anchor.hasOwnProperty(id)){
		//devices.anchor[id].status = 'active';
		devices.anchor[id].isActive = true;
		devices.anchor[id].lastUpdate = getUnixTime();
	}
	//console.log(devices.anchor);
	return true
    //console.log(Object.keys(fifoData.anchors).length);
    //return fifoData.anchors;
}

function getAliveAnchors(intervalInSec){
	var aliveAnchors = [];
	var aliveInterval = 0;
	var currentTime = getUnixTime();

	if(!intervalInSec){
		intervalInSec = 30;
	}

	var ignoreAlive = false;
	if(intervalInSec < 0){
		ignoreAlive = true;
	}

	for(var key in devices.anchor){
		// skip loop if the property is from prototype
    	if (!devices.anchor.hasOwnProperty(key)) continue;

    	var anchor = devices.anchor[key];
    	if(ignoreAlive){
    		aliveInterval = intervalInSec - 1;
    	}else{
    		aliveInterval = currentTime - anchor.lastUpdate;
    	}
    	
    	if(anchor.isActive && (aliveInterval <= intervalInSec)){
    		aliveAnchors.push(anchor);
    	}
	}
	//console.log(aliveAnchors.length);
	return aliveAnchors;
}

function filterData(datas, schema){
	var results = [];
	let match = 0;
	var isArray = Array.isArray(datas);
	var isObject = false;
	if(typeof datas === 'object' && datas !== null){
		isObject = true;
		isArray = false;
	}

	if(isArray){
		datas.forEach(function(data, index){
			match = 0;
			for(var schemaKey in schema){
				if(data.hasOwnProperty(schemaKey)){
					if(data[schemaKey] == schema[schemaKey]){
						match++;
					}
				}
			}
			if(match == Object.keys(schema).length){
				results.push(data);
			}
		});
	}

	if(isObject){
		for(var key in datas){
			let data = datas[key];
			match = 0;
			for(var schemaKey in schema){
				if(data.hasOwnProperty(schemaKey)){
					if(data[schemaKey] == schema[schemaKey]){
						match++;
					}
				}
			}
			if(match == Object.keys(schema).length){
				results.push(data);
			}
		}
	}
	return results;
}

function filterAnchors(schema){
	var results = [];
	for(var key in devices.anchor){
		let anchor = devices.anchor[key];

		for(var schemaKey in schema){
			if(anchor.hasOwnProperty(schemaKey)){
				if(anchor[schemaKey] == schema[schemaKey]){
					results.push(anchor);
				}
			}
		}
	}
	return results;
}

function getAnchorByLevel(levelId, aliveInterval){
	var results = [];
	var aliveAnchors;

	if(aliveInterval){
		aliveAnchors = getAliveAnchors(aliveInterval);
	}else{
		aliveAnchors = getAliveAnchors(-1);
	}

	aliveAnchors.forEach(function(aliveAnchor, index){
		if(aliveAnchor.level == levelId){
			results.push(aliveAnchor);
		}
	});

	return results;
}

function getDevices(){
	return devices;
}

function getUnixTime(){
    var date = new Date();
    var milliUnixTime = date.getTime(); //in milliseconds
    return Math.round(milliUnixTime/1000); //in seconds
}

function updateTagTimestamp(tagId){
  if(devices.tag.hasOwnProperty(tagId)){
    devices.tag[tagId].lastUpdate = getUnixTime();
    return true;
  }
  return false;
}

function isActiveTag(tagId, limit){
  	if(devices.tag.hasOwnProperty(tagId)){
  		let datas = tagId.split('-');
  		let minor = datas[datas.length-1];

  		if(limit != null){
			if(minor <= limit){
  				return devices.tag[tagId].isActive;
  			}else{
  				return null;
  			}
  		}
  		return devices.tag[tagId].isActive;
	}
	return null;
}

function getAnchorLevel(anchorId){
  if(devices.anchor.hasOwnProperty(anchorId))
    return devices.anchor[anchorId].level;
  else
    return null;
}

function getAnchorLabel(anchorId){
  if(devices.anchor.hasOwnProperty(anchorId))
    return devices.anchor[anchorId].label;
  else
    return null;
}

function getAnchorById(anchorId){
	if(devices.anchor.hasOwnProperty(anchorId)){
	  return devices.anchor[anchorId];
	}
	return null;
}

function setTagLevel(tagId, level){
  if(devices.tag.hasOwnProperty(tagId)){
    devices.tag[tagId].level = level;
    return true;
  }
  return false;
}

/*loadProfiles(function(profiles){
	console.log(profiles)
});*/

var methods = {};
methods.loadProfiles = loadProfiles;
methods.getDevices = getDevices;
methods.updateTagTimestamp = updateTagTimestamp;
methods.isActiveTag = isActiveTag;
methods.getAnchorLevel = getAnchorLevel;
methods.getAnchorLabel = getAnchorLabel;
methods.getAnchorById = getAnchorById;
methods.setTagLevel = setTagLevel;

methods.updateAnchor = updateAnchor;
methods.getAliveAnchors = getAliveAnchors;
methods.getAnchorByLevel = getAnchorByLevel;
methods.filterAnchors = filterAnchors;
methods.filterData = filterData;
methods.filterArray = filterArray;

methods.getJoinUser = getJoinUser;
methods.filterJoinUser = filterJoinUser;
methods.getFloor = getFloor;
methods.getUserOnFloor = getUserOnFloor;
methods.setUserLastFloor = setUserLastFloor;
methods.updateStatusUser = updateStatusUser;
methods.checkAnchor = checkAnchor;

//exports.methods = methods;
module.exports = methods;